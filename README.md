# Viihde Toolkit

A collection of tools to be used with Elisa Viihde.

## Tools

### Grabber

Allows for downloading of your recordings in `.ts` form, which can be played or converted locally.

### Dupe Search

Searches for recordings of the same content based on their descriptions and allows for bulk deletion of duplicates.

### Sandbox

Simplifies testing queries to the Elisa Viihde API.

## Libraries Required

- [Apache HTTPComponents](http://hc.apache.org/) (`httpclient.jar`, `httpcore.jar`, `httpmime.jar`, `fluent-hc.jar`, `commons-codec.jar` and `commons-logging.jar`)
- [JSON.simple](https://code.google.com/p/json-simple/) (`json-simple.jar`)