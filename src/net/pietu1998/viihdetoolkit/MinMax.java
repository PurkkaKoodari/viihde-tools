package net.pietu1998.viihdetoolkit;

import java.util.Comparator;

public class MinMax<E> {
	
	private Comparator<? super E> comparator;
	private E min = null;
	private E max = null;
	private boolean empty = false;
	
	public MinMax() {
		this((E) null);
		this.empty = true;
	}
	
	public MinMax(Comparator<? super E> comparator) {
		this(null, comparator);
		this.empty = true;
	}
	
	public MinMax(E initial) {
		this(initial, initial);
	}
	
	public MinMax(E initial, Comparator<? super E> comparator) {
		this(initial, initial, comparator);
	}
	
	public MinMax(E min, E max) {
		this(min, max, new DefaultComparator<E>());
	}
	
	public MinMax(E min, E max, Comparator<? super E> comparator) {
		this.min = min;
		this.max = max;
		this.comparator = comparator;
	}
	
	public void update(E item) {
		if (empty || comparator.compare(item, min) < 0)
			min = item;
		if (empty || comparator.compare(item, max) > 0)
			max = item;
		empty = false;
	}
	
	public boolean isEmpty() {
		return empty;
	}
	
	public E getMin() {
		if (empty)
			throw new IllegalStateException("empty minmax, no min");
		return min;
	}
	
	public E getMax() {
		if (empty)
			throw new IllegalStateException("empty minmax, no max");
		return max;
	}
	
	private static class DefaultComparator<E> implements Comparator<E> {

		@SuppressWarnings("unchecked")
		@Override
		public int compare(E o1, E o2) {
			return ((Comparable<? super E>) o1).compareTo(o2);
		}
		
	}

}
