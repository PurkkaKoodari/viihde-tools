package net.pietu1998.viihdetoolkit;

public interface ViihdeApplication {
	
	public static final String API_URL = "http://api.elisaviihde.fi/etvrecorder/";

	public void startApplication();

}
