package net.pietu1998.viihdetoolkit;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.filechooser.FileFilter;

import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ElisaViihdeGrabber extends JFrame implements ViihdeApplication {

	private static final long serialVersionUID = 7705555376558957071L;

	public static void main(String[] args) {
		new ElisaViihdeGrabber().kaynnista();
	}

	public void startApplication() {
		kaynnista();
	}

	DefaultListModel<Tallenne> model;
	JList<Tallenne> tallenteet;
	JLabel tila;
	Timer paivitysAjastin;
	boolean sisalla = false, poistuKunValmis = false;
	String kansio, tulevaKansio;
	CookieStore yleisKeksiVarasto;
	HttpContext yleisKonteksti;
	Lataa tallennin = null;
	Tallenne tallenne = null;
	long[] luetut;
	int[] maarat;

	public static final int SPEED_BUFFER = 20;

	public ElisaViihdeGrabber() {
		super();
	}

	private void kaynnista() {
		setTitle("Elisa Viihde Grabber");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		setLocationByPlatform(true);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				if (sisalla) {
					if (tallennin != null) {
						int vastaus = JOptionPane.showConfirmDialog(ElisaViihdeGrabber.this,
								"Lataat parhaillaan ohjelmaa. Haluatko peruuttaa ja sulkea sovelluksen?", "Peruuta?",
								JOptionPane.YES_NO_OPTION);
						if (vastaus != JOptionPane.YES_OPTION)
							return;
						tallennin.peruuta = true;
						paivitysAjastin.stop();
					}
					tila.setText("Kirjaudutaan ulos");
					URI uri = uri(API_URL + "logout.sl?ajax=true");
					new Lataa(uri, new Valmis() {
						public void valmis(Lataa l) {
							if (tallennin != null)
								poistuKunValmis = true;
							else
								System.exit(0);
						}
					}, false).start();
				} else
					System.exit(0);
			}
		});
		try {
			List<Image> icons = new ArrayList<Image>();
			icons.add(ImageIO.read(getClass().getResourceAsStream("/icon-grabber.png")));
			icons.add(ImageIO.read(getClass().getResourceAsStream("/icon-grabber16.png")));
			setIconImages(icons);
		} catch (IOException e) {
			virheIlmoitus(e);
			System.exit(1);
		}
		model = new DefaultListModel<Tallenne>();
		tallenteet = new JList<Tallenne>(model);
		tallenteet.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tallenteet.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					Tallenne t = tallenteet.getSelectedValue();
					if (t == null)
						return;
					if (t.kansio) {
						meneKansioon(t.id);
					} else {
						tallenna(t);
					}
				}
			}
		});
		tila = new JLabel("");
		JScrollPane skrolli = new JScrollPane(tallenteet);
		add(skrolli, BorderLayout.CENTER);
		add(tila, BorderLayout.PAGE_END);
		setSize(640, 480);
		setVisible(true);
		tila.setText("Odottaa tunnuksia");
		String kayttaja = JOptionPane.showInputDialog(this, "Käyttäjänimi:", "Käyttäjänimi",
				JOptionPane.QUESTION_MESSAGE);
		if (kayttaja == null)
			System.exit(0);
		String salasana = JOptionPane.showInputDialog(this, "Salasana:", "Salasana", JOptionPane.QUESTION_MESSAGE);
		if (salasana == null)
			System.exit(0);
		tila.setText("Kirjaudutaan sisään");
		URI uri = uri(API_URL + "default.sl?username=" + kayttaja + "&password=" + salasana + "&ajax=true");
		new Lataa(uri, new Valmis() {
			public void valmis(Lataa l) {
				loginValmis(l);
			}
		}, true).start();
	}

	private void loginValmis(Lataa l) {
		if (l.tulos.equalsIgnoreCase("true")) {
			sisalla = true;
			meneKansioon("");
		} else {
			JOptionPane
					.showMessageDialog(this, "Väärä käyttäjänimi tai salasana", "Virhe", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
	}

	private void kansioLadattu(Lataa l) {
		tila.setText("Valmis");
		kansio = tulevaKansio;
		JSONParser parseri = new JSONParser();
		try {
			JSONObject ulos = (JSONObject) parseri.parse(URLDecoder.decode(l.tulos, "utf-8"));
			final JSONObject data = (JSONObject) ((JSONArray) ulos.get("ready_data")).get(0);
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					model.clear();
					if (!kansio.equals("")) {
						model.addElement(new Paakansio());
					}
					lisaaTallenteet((JSONArray) data.get("folders"), true);
					lisaaTallenteet((JSONArray) data.get("recordings"), false);
				}
			});
		} catch (Exception e) {
			virheIlmoitus(e);
		}
	}

	private void lisaaTallenteet(JSONArray taul, boolean kansio) {
		for (Object o : taul) {
			JSONObject jo = (JSONObject) o;
			Tallenne t = new Tallenne();
			t.id = (String) jo.get("id");
			t.nimi = (String) jo.get("name");
			t.kansio = kansio;
			if (kansio) {
				t.kesto = (String) jo.get("recordings_count");
				t.koko = (String) jo.get("size");
			} else {
				t.pid = (String) jo.get("program_id");
				t.kanava = (String) jo.get("channel");
				t.kesto = (String) jo.get("length") + " min";
			}
			model.addElement(t);
		}
	}

	private void meneKansioon(String id) {
		tila.setText("Ladataan kansiota");
		tulevaKansio = id;
		URI uri = uri(API_URL + "ready.sl?folderid=" + id + "&ajax=true");
		new Lataa(uri, new Valmis() {
			public void valmis(Lataa l) {
				kansioLadattu(l);
			}
		}, false).start();
	}

	private void tallenna(Tallenne t) {
		if (tallennin != null) {
			JOptionPane.showMessageDialog(this,
					"Lataat jo ohjelmaa. Grabber tukee vain yhden ohjelman latausta kerrallaan.", "Virhe",
					JOptionPane.WARNING_MESSAGE);
			return;
		}
		int vastaus = JOptionPane.showConfirmDialog(ElisaViihdeGrabber.this, "Haluatko tallentaa tämän ohjelman?",
				"Tallennus", JOptionPane.YES_NO_OPTION);
		if (vastaus != JOptionPane.YES_OPTION)
			return;
		tila.setText("Ladataan tietoja");
		URI uri = uri(API_URL + "program.sl?programid=" + t.pid + "&ajax=true");
		new Lataa(uri, new Valmis() {
			public void valmis(Lataa l) {
				tallenna(l);
			}
		}, false).start();
		tallenne = t;
	}

	private void tallenna(Lataa l) {
		JSONParser parseri = new JSONParser();
		try {
			JSONObject ulos = (JSONObject) parseri.parse(URLDecoder.decode(l.tulos, "utf-8"));
			URI uri = uri((String) ulos.get("url"));
			tila.setText("Odotetaan tiedoston valintaa");
			JFileChooser tiedosto = new JFileChooser();
			tiedosto.setFileFilter(new FileFilter() {
				@Override
				public String getDescription() {
					return ".ts -tiedostot";
				}

				@Override
				public boolean accept(File arg0) {
					return arg0.getName().endsWith(".ts");
				}
			});
			int vastaus = tiedosto.showSaveDialog(this);
			if (vastaus == JFileChooser.APPROVE_OPTION) {
				luetut = new long[SPEED_BUFFER];
				maarat = new int[SPEED_BUFFER];
				paivitysAjastin = new Timer(1000, new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						paivitaTila();
					}
				});
				tallennin = new Lataa(uri, new Valmis() {
					public void valmis(Lataa l) {
						paivitysAjastin.stop();
						boolean peruuta = tallennin.peruuta;
						tallennin = null;
						if (poistuKunValmis)
							System.exit(0);
						else if (!peruuta)
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									tila.setText("Valmis");
								}
							});
					}
				}, false);
				tallennin.tulosTied = tiedosto.getSelectedFile();
				tallennin.start();
				paivitysAjastin.setInitialDelay(100);
				paivitysAjastin.start();
			}
		} catch (Exception e) {
			virheIlmoitus(e);
		}
	}

	private void paivitaTila() {
		if (tallennin.totaaliKoko > 0) {
			double prosentti = 100.0 * tallennin.totaaliLuettu / tallennin.totaaliKoko;
			int keskiarvo = 0;
			for (int i = 0; i < SPEED_BUFFER - 1; i++) {
				luetut[i] = luetut[i + 1];
				maarat[i] = maarat[i + 1];
				keskiarvo += maarat[i];
			}
			luetut[SPEED_BUFFER - 1] = tallennin.totaaliLuettu;
			maarat[SPEED_BUFFER - 1] = (int) (luetut[SPEED_BUFFER - 1] - luetut[SPEED_BUFFER - 2]);
			keskiarvo += maarat[SPEED_BUFFER - 1];
			keskiarvo /= 10;
			long aika = (long) ((tallennin.totaaliKoko - tallennin.totaaliLuettu) / Math.max(1, keskiarvo));
			tila.setText(String.format("Ladataan %s: %s/%s (%3.2f %%) %s/s Aikaa j�ljell�: %s", tallenne.nimi,
					koko(tallennin.totaaliLuettu), koko(tallennin.totaaliKoko), prosentti,
					koko(maarat[SPEED_BUFFER - 1]), aika(aika)));
		}
	}

	private String koko(long num) {
		String[] maarat = new String[] { "t", "kt", "Mt", "Gt", "Tt" };
		int potenssi = 0;
		while (num > 1000) {
			num /= 1000;
			potenssi++;
		}
		return num + " " + maarat[potenssi];
	}

	private String aika(long num) {
		if (num > 86400) {
			return (num / 86400) + " päivää " + ((num % 86400) / 3600) + " tuntia";
		}
		if (num > 3600) {
			return (num / 3600) + " tuntia " + ((num % 3600) / 60) + " minuuttia";
		}
		if (num > 60) {
			return (num / 60) + " minuuttia " + (num % 60) + " sekuntia";
		}
		return num + " sekuntia";
	}

	private URI uri(String str) {
		try {
			return new URI(str);
		} catch (Exception e) {
			virheIlmoitus(e);
			return null;
		}
	}

	private void virheIlmoitus(Throwable t) {
		final StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		Runnable r = new Runnable() {
			public void run() {
				JOptionPane.showMessageDialog(ElisaViihdeGrabber.this, sw.toString(), "Virhe",
						JOptionPane.ERROR_MESSAGE);
			}
		};
		if (SwingUtilities.isEventDispatchThread())
			r.run();
		else
			SwingUtilities.invokeLater(r);
	}

	private class Lataa extends Thread {
		URI uri;
		String tulos;
		Valmis valmis;
		boolean uusiKonteksti, peruuta;
		File tulosTied = null;
		long totaaliKoko, totaaliLuettu;

		public Lataa(URI uri, Valmis valmis, boolean uusiKonteksti) {
			this.uri = uri;
			this.valmis = valmis;
			this.uusiKonteksti = uusiKonteksti;
		}

		@Override
		public void run() {
			try {
				BufferedReader reader = null;
				HttpClient asiakas = HttpClients.createDefault();
				HttpGet get = new HttpGet(uri);
				if (uusiKonteksti) {
					HttpContext konteksti = yleisKonteksti = new BasicHttpContext();
					CookieStore keksiVarasto = yleisKeksiVarasto = new BasicCookieStore();
					konteksti.setAttribute("http.cookie-store", keksiVarasto);
				}
				CloseableHttpResponse vastaus = (CloseableHttpResponse) asiakas.execute(get, yleisKonteksti);
				InputStream in = vastaus.getEntity().getContent();
				if (tulosTied == null) {
					InputStreamReader inputstreamreader = new InputStreamReader(in, "utf-8");
					reader = new BufferedReader(inputstreamreader);
					StringBuffer puskuri = new StringBuffer();
					String s = "";
					while (s != null) {
						puskuri.append(s);
						s = reader.readLine();
					}
					reader.close();
					tulos = puskuri.toString();
				} else {
					OutputStream out = new FileOutputStream(tulosTied);
					totaaliKoko = Long.parseLong(vastaus.getFirstHeader("Content-Length").getValue());
					totaaliLuettu = 0;
					int luettu = 0;
					byte[] puskuri = new byte[1024];
					while ((luettu = in.read(puskuri)) > -1 && !peruuta) {
						out.write(puskuri, 0, luettu);
						totaaliLuettu += luettu;
					}
					vastaus.close();
					in.close();
					out.close();
					if (peruuta)
						tulosTied.delete();
				}
				valmis.valmis(this);
			} catch (Exception e) {
				virheIlmoitus(e);
				valmis.valmis(this);
			}
		}
	}

	private interface Valmis {
		abstract void valmis(Lataa l);
	}

	private class Tallenne {
		boolean kansio;
		String id, pid, nimi, kesto, koko, kanava;

		@Override
		public String toString() {
			return nimi + " (" + ((kansio) ? "kansio, " + kesto + " tallennetta, " + koko : kanava + ", " + kesto)
					+ ")";
		}
	}

	private class Paakansio extends Tallenne {
		public Paakansio() {
			id = "";
			kansio = true;
		}

		@Override
		public String toString() {
			return "Palaa pääkansioon";
		}
	}
}
