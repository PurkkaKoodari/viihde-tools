package net.pietu1998.viihdetoolkit;

import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;

public class ElisaViihdeToolkit extends JFrame implements ViihdeApplication {

	private static final long serialVersionUID = 4010826548044082776L;

	public static void main(String[] args) {
		new ElisaViihdeToolkit().kaynnista(args);
	}

	public void startApplication() {
		kaynnista(new String[] {});
	}

	private static final Map<String, Class<? extends ViihdeApplication>> ohjelmat = new LinkedHashMap<String, Class<? extends ViihdeApplication>>();

	static {
		ohjelmat.put("Grabber", ElisaViihdeGrabber.class);
		ohjelmat.put("Dupe Search", ElisaViihdeDupeSearch.class);
		ohjelmat.put("Sandbox", ElisaViihdeSandbox.class);
	}

	public ElisaViihdeToolkit() throws HeadlessException {
		super();
	}

	private void kaynnista(String[] args) {
		if (args.length > 0) {
			String ohjelma = args[0].replace('_', ' ');
			if (ohjelmat.containsKey(ohjelma)) {
				aja(ohjelma);
				return;
			} else {
				StringBuilder sb = new StringBuilder("Virheellinen ohjelma '" + ohjelma + "'. Mahdolliset ohjelmat:");
				for (String s : ohjelmat.keySet())
					sb.append("\n").append(s.replace(' ', '_'));
				JOptionPane.showMessageDialog(this, sb.toString(), "Virhe", JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
		}
		setTitle("Elisa Viihde Toolkit");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(200, 200);
		setLocationByPlatform(true);
		setResizable(false);
		try {
			List<Image> icons = new ArrayList<Image>();
			icons.add(ImageIO.read(getClass().getResourceAsStream("/icon-toolkit.png")));
			icons.add(ImageIO.read(getClass().getResourceAsStream("/icon-toolkit16.png")));
			setIconImages(icons);
		} catch (IOException | IllegalArgumentException e) {
			JOptionPane.showMessageDialog(ElisaViihdeToolkit.this, "Käynnistys epäonnistui: " + e.getClass().getName());
			System.exit(1);
		}
		String[] taulukko = new String[ohjelmat.size()];
		ohjelmat.keySet().toArray(taulukko);
		final JList<String> lista = new JList<String>(taulukko);
		lista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lista.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				String ohjelma = lista.getSelectedValue();
				if (ohjelma != null && e.getClickCount() == 2) {
					remove(lista);
					setVisible(false);
					aja(ohjelma);
				}
			}
		});
		add(lista);
		setVisible(true);
	}

	private void aja(String ohjelma) {
		try {
			ohjelmat.get(ohjelma).newInstance().startApplication();
		} catch (Exception ex) {
			JOptionPane
					.showMessageDialog(ElisaViihdeToolkit.this, "Käynnistys epäonnistui: " + ex.getClass().getName());
		}
	}

}
