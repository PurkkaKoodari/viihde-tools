package net.pietu1998.viihdetoolkit;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ElisaViihdeDupeSearch extends JFrame implements ViihdeApplication {

	private static final long serialVersionUID = 6965610456515679312L;

	public static void main(String[] args) {
		new ElisaViihdeDupeSearch().launch();
	}

	public void startApplication() {
		launch();
	}

	private RefreshableListModel duplicatesModel;
	private DefaultListModel<Recording> recordingsModel;
	private JList<Duplicate> duplicatesList;
	private JList<Recording> recordingsList;
	private JLabel statusLabel;
	private JTextArea detailsTextArea;

	private Map<String, Recording> recordings = new HashMap<>();
	private List<Duplicate> duplicates = new ArrayList<>();
	private Map<String, String> folderNames = new HashMap<>();

	private List<Recording> shownRecordings = new ArrayList<>();
	private List<Duplicate> chosenDuplicates = new ArrayList<>();

	private boolean loggedIn = false;

	private volatile int recordingsToLoad, loadedRecordings;

	private volatile CookieStore cookieStore;
	private volatile HttpContext httpContext;

	private LoaderThread[] loaderThreads = new LoaderThread[THREADS];
	private volatile ConcurrentLinkedQueue<LoadTask> tasks = new ConcurrentLinkedQueue<>();
	private volatile Lock mainLock = new ReentrantLock();
	private volatile Condition noTasks = mainLock.newCondition();

	public static final int THRESHOLD = 50;
	public static final int THREADS = 16;

	public ElisaViihdeDupeSearch() {
		super();
	}

	private void launch() {
		setTitle("Elisa Viihde Dupe Search");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setSize(800, 600);
		setLocationByPlatform(true);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				if (loggedIn) {
					loggedIn = false;
					statusLabel.setText("Kirjaudutaan ulos");
					tasks.clear();
					addLoadTask(new LoadTask(API_URL + "logout.sl?ajax=true", new LoadEvent() {
						public void loadCompleted(LoaderThread thread, boolean success) {
							interruptThreads();
							System.exit(0);
						}
					}));
				} else {
					System.exit(0);
				}
			}
		});
		try {
			List<Image> icons = new ArrayList<Image>();
			icons.add(ImageIO.read(getClass().getResourceAsStream("/icon-dupesearch.png")));
			icons.add(ImageIO.read(getClass().getResourceAsStream("/icon-dupesearch16.png")));
			setIconImages(icons);
		} catch (IOException e) {
			showError(e);
			System.exit(1);
		}
		final JButton deleteExtraButton = new JButton("Poista turhat kopiot");
		deleteExtraButton.setEnabled(false);
		deleteExtraButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Duplicate> selectedDuplicates = duplicatesList.getSelectedValuesList();
				if (selectedDuplicates.isEmpty())
					return;
				deleteExtraCopies(selectedDuplicates);
			}
		});
		final JButton deleteRecordingsButton = new JButton("Poista");
		deleteRecordingsButton.setEnabled(false);
		deleteRecordingsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Recording> selectedRecordings = recordingsList.getSelectedValuesList();
				if (selectedRecordings.isEmpty())
					return;
				deleteRecordings(selectedRecordings);
			}
		});
		recordingsModel = new DefaultListModel<Recording>();
		recordingsList = new JList<Recording>(recordingsModel);
		recordingsList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		recordingsList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					List<Recording> selectedRecordings = recordingsList.getSelectedValuesList();
					if (selectedRecordings.isEmpty())
						return;
					deleteRecordings(selectedRecordings);
				}
			}
		});
		recordingsList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				List<Recording> selectedRecordings = recordingsList.getSelectedValuesList();
				if (selectedRecordings.isEmpty()) {
					deleteRecordingsButton.setEnabled(false);
					selectedRecordings = shownRecordings;
				} else
					deleteRecordingsButton.setEnabled(true);
				detailsTextArea.setText(generateDetails(selectedRecordings));
			}
		});
		duplicatesModel = new RefreshableListModel();
		duplicatesList = new JList<Duplicate>(duplicatesModel);
		duplicatesList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		duplicatesList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					List<Duplicate> selectedDuplicates = duplicatesList.getSelectedValuesList();
					if (selectedDuplicates.isEmpty())
						return;
					deleteExtraCopies(selectedDuplicates);
				}
			}
		});
		duplicatesList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				chosenDuplicates = duplicatesList.getSelectedValuesList();
				refreshRecordingsList();
				if (chosenDuplicates.isEmpty()) {
					deleteExtraButton.setEnabled(false);
					return;
				}
				deleteExtraButton.setEnabled(true);
			}
		});
		detailsTextArea = new JTextArea();
		detailsTextArea.setEditable(false);
		detailsTextArea.setLineWrap(true);
		detailsTextArea.setWrapStyleWord(true);
		statusLabel = new JLabel("");
		JPanel recordingsPanel = new JPanel(new BorderLayout());
		JPanel detailsPanel = new JPanel(new BorderLayout());
		JScrollPane duplicatesScroll = new JScrollPane(duplicatesList);
		JScrollPane recordingsScroll = new JScrollPane(recordingsList);
		JScrollPane detailsScroll = new JScrollPane(detailsTextArea);
		recordingsPanel.add(recordingsScroll, BorderLayout.CENTER);
		recordingsPanel.add(deleteExtraButton, BorderLayout.PAGE_END);
		detailsPanel.add(detailsScroll, BorderLayout.CENTER);
		detailsPanel.add(deleteRecordingsButton, BorderLayout.PAGE_END);
		final JSplitPane leftSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		final JSplitPane rightSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		leftSplit.setLeftComponent(duplicatesScroll);
		rightSplit.setLeftComponent(recordingsPanel);
		rightSplit.setRightComponent(detailsPanel);
		leftSplit.setRightComponent(rightSplit);
		add(leftSplit, BorderLayout.CENTER);
		add(statusLabel, BorderLayout.PAGE_END);
		setVisible(true);
		rightSplit.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				leftSplit.setDividerLocation(0.2);
				rightSplit.setDividerLocation(0.375);
				rightSplit.setResizeWeight(1);
			}
		});
		setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
		statusLabel.setText("Odottaa tunnuksia");
		String username = JOptionPane.showInputDialog(this, "Käyttäjänimi:", "Käyttäjänimi",
				JOptionPane.QUESTION_MESSAGE);
		if (username == null)
			System.exit(0);
		String password = JOptionPane.showInputDialog(this, "Salasana:", "Salasana", JOptionPane.QUESTION_MESSAGE);
		if (password == null)
			System.exit(0);
		statusLabel.setText("Kirjaudutaan sisään");
		createThreads();
		addLoadTask(new LoadTask(API_URL + "default.sl?username=" + username + "&password=" + password + "&ajax=true",
				new LoadEvent() {
					public void loadCompleted(LoaderThread thread, boolean success) {
						loginComplete(thread);
					}
				}));
		folderNames.put("", "Pääkansio");
	}

	private String generateDetails(List<Recording> recordings) {
		if (recordings.isEmpty()) {
			return "";
		}
		String name = null, description = null;
		SortedSet<String> channels = new TreeSet<>(new NaturalOrderComparator());
		MinMax<Date> dates = new MinMax<>();
		MinMax<Duration> durations = new MinMax<>();
		SortedSet<String> folders = new TreeSet<>(new NaturalOrderComparator());
		MinMax<Integer> views = new MinMax<>();
		MinMax<Integer> copies = new MinMax<>();
		for (Recording recording : recordings) {
			if (name == null)
				name = recording.name;
			else if (!name.equals(recording.name))
				name = "(useita nimiä)";
			if (description == null)
				description = recording.description;
			else if (!description.equals(recording.description))
				description = "(useita kuvauksia)";
			channels.add(recording.channel);
			dates.update(recording.date);
			durations.update(recording.duration);
			folders.add(recording.folderName);
			views.update(recording.viewCount);
			if (recording.duplicate != null)
				copies.update(recording.duplicate.parts.size());
		}
		StringBuilder sb = new StringBuilder();
		sb.append(name).append("\n");
		Iterator<String> iter = channels.iterator();
		sb.append(iter.next());
		while (iter.hasNext())
			sb.append(", ").append(iter.next());
		sb.append("\n");
		Date dateMin = dates.getMin(), dateMax = dates.getMax();
		if (dateMin.equals(dateMax))
			sb.append(DETAILS_FORMAT.format(dateMin)).append("\n");
		else
			sb.append(DETAILS_FORMAT.format(dateMin)).append(" - ").append(DETAILS_FORMAT.format(dateMax)).append("\n");
		Duration durationMin = durations.getMin(), durationMax = durations.getMax();
		if (dateMin.equals(dateMax))
			sb.append(durationMin).append("\n");
		else
			sb.append(durationMin).append(" - ").append(durationMax).append("\n");
		if (folders.size() == 1)
			sb.append("Kansiossa ").append(folders.first()).append("\n");
		else {
			sb.append("Kansioissa ");
			iter = folders.iterator();
			sb.append(iter.next());
			while (iter.hasNext())
				sb.append(", ").append(iter.next());
			sb.append("\n");
		}
		sb.append("Katsottu ");
		int viewsMin = views.getMin(), viewsMax = views.getMax();
		if (viewsMin == viewsMax)
			if (viewsMin == 1)
				sb.append("kerran\n");
			else
				sb.append(viewsMin).append(" kertaa\n");
		else
			sb.append(viewsMin).append("-").append(viewsMax).append(" kertaa\n");
		int copiesMin = copies.getMin(), copiesMax = copies.getMax();
		if (copiesMin == copiesMax)
			if (copiesMin == 1)
				sb.append(copiesMin).append(" kopio");
			else
				sb.append(copiesMin).append(" kopiota");
		else
			sb.append(copiesMin).append("-").append(copiesMax).append(" kopiota");
		sb.append("\n\nKuvaus:\n").append(description);
		return sb.toString();
	}

	private void refreshRecordingsList() {
		detailsTextArea.setText("");
		recordingsModel.clear();
		shownRecordings.clear();
		for (Duplicate duplicate : chosenDuplicates) {
			for (Recording recording : duplicate.parts) {
				recordingsModel.addElement(recording);
				shownRecordings.add(recording);
			}
		}
		detailsTextArea.setText(generateDetails(shownRecordings));
	}

	private void deleteRecording(Recording recording, boolean skipPrompt, final int totalToDelete,
			final AtomicInteger itemsToDelete) {
		if (recording == null)
			return;
		int option = JOptionPane.YES_OPTION;
		if (!skipPrompt)
			option = JOptionPane.showConfirmDialog(this, "Poistetaanko " + recording.name + "?", "Poista",
					JOptionPane.YES_NO_OPTION);
		if (option == JOptionPane.YES_OPTION) {
			final Recording toDelete = recording;
			final Duplicate deleteOwner = toDelete.duplicate;
			final int deletePos = duplicatesModel.indexOf(deleteOwner);
			statusLabel.setText("Poistetaan " + recording.name);
			addLoadTask(new LoadTask(API_URL + "program.sl?remove=true&removep=" + recording.programViewId
					+ "&ajax=true", new LoadEvent() {
				public void loadCompleted(LoaderThread thread, boolean success) {
					deleteComplete(thread, success, deletePos, toDelete, totalToDelete, itemsToDelete);
				}
			}));
		}
	}

	private void deleteRecordings(List<Recording> toDelete) {
		int option = JOptionPane.showConfirmDialog(this, "Poistetaanko " + toDelete.size() + " tallenne(tta)?",
				"Poista", JOptionPane.YES_NO_OPTION);
		if (option == JOptionPane.YES_OPTION) {
			AtomicInteger itemsToDelete = new AtomicInteger(toDelete.size());
			for (Recording recording : toDelete) {
				deleteRecording(recording, true, toDelete.size(), itemsToDelete);
			}
		}
	}

	private void deleteExtraCopies(List<Duplicate> duplicates) {
		List<Recording> toDelete = new ArrayList<>();
		for (Duplicate duplicate : duplicates) {
			Recording toKeep = null;
			for (Recording recording : duplicate.parts) {
				if (toKeep == null) {
					toKeep = recording;
				} else if (recording.isBetterThan(toKeep)) {
					toDelete.add(toKeep);
					toKeep = recording;
				} else {
					toDelete.add(recording);
				}
			}
		}
		deleteRecordings(toDelete);
	}

	private void deleteComplete(LoaderThread thread, boolean success, int deletePos, final Recording toDelete,
			final int totalToDelete, AtomicInteger itemsToDelete) {
		if (success && thread.result.equalsIgnoreCase("true")) {
			Duplicate deleteOwner = toDelete.duplicate;
			deleteOwner.parts.remove(toDelete);
			if (itemsToDelete.decrementAndGet() == 0) {
				duplicatesModel.refresh();
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						refreshRecordingsList();
						statusLabel.setText("Valmis");
						JOptionPane.showMessageDialog(ElisaViihdeDupeSearch.this, "Poistettu " + totalToDelete
								+ " kopio(ta).", "Poistettu", JOptionPane.INFORMATION_MESSAGE);
					}
				});
			}
		} else {
			JOptionPane.showMessageDialog(this, "Poisto epäonnistui.", "Virhe", JOptionPane.WARNING_MESSAGE);
		}
	}

	private void loginComplete(LoaderThread thread) {
		if (thread.result.equalsIgnoreCase("true")) {
			loggedIn = true;
			loadFolders();
		} else {
			JOptionPane
					.showMessageDialog(this, "Väärä käyttäjänimi tai salasana", "Virhe", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
	}

	private void folderLoaded(final LoaderThread thread, boolean success) {
		if (success) {
			JSONParser parser = new JSONParser();
			try {
				JSONObject parsed = (JSONObject) parser.parse(thread.result);
				JSONObject data = (JSONObject) ((JSONArray) parsed.get("ready_data")).get(0);
				addFolders((JSONArray) data.get("folders"), thread);
				addRecordings((JSONArray) data.get("recordings"));
			} catch (ParseException e) {} catch (Exception e) {
				showError(e);
			}
			loadedRecordings++;
			statusLoadFolders();
			if (loadedRecordings >= recordingsToLoad) {
				loadRecordings();
			}
		} else {
			addLoadTask(thread.task);
		}
	}

	private void addRecordings(JSONArray arr) throws UnsupportedEncodingException, NumberFormatException {
		for (Object obj : arr) {
			JSONObject jo = (JSONObject) obj;
			Recording rec = new Recording();
			rec.programViewId = (String) jo.get("id");
			rec.name = URLDecoder.decode((String) jo.get("name"), "utf-8");
			rec.programId = (String) jo.get("program_id");
			rec.viewCount = Integer.parseInt((String) jo.get("viewcount"));
			rec.folderId = (String) jo.get("folder_id");
			rec.duration = new Duration(Integer.parseInt((String) jo.get("length")));
			recordings.put(rec.programViewId, rec);
		}
	}

	private void addFolders(JSONArray arr, LoaderThread thread) throws UnsupportedEncodingException {
		for (Object o : arr) {
			recordingsToLoad++;
			JSONObject jo = (JSONObject) o;
			String id = (String) jo.get("id");
			folderNames.put(id, URLDecoder.decode((String) jo.get("name"), "utf-8"));
			addLoadTask(new LoadTask(API_URL + "ready.sl?folderid=" + id + "&ajax=true", new LoadEvent() {
				public void loadCompleted(LoaderThread thread, boolean success) {
					folderLoaded(thread, success);
				}
			}));
		}
	}

	private synchronized void recordingLoaded(LoaderThread thread, boolean success) {
		if (success) {
			JSONParser parser = new JSONParser();
			try {
				final JSONObject data = (JSONObject) parser.parse(thread.result);
				Recording recording = recordings.get((String) data.get("programviewid"));
				if (recording != null) {
					recording.description = URLDecoder.decode((String) data.get("short_text"), "utf-8");
					recording.channel = URLDecoder.decode((String) data.get("channel"), "utf-8");
					recording.dateString = URLDecoder.decode((String) data.get("start_time"), "utf-8");
					recording.duration.text = URLDecoder.decode((String) data.get("flength"), "utf-8");
					recording.update(folderNames);
				}
			} catch (ParseException e) {} catch (Exception e) {
				showError(e);
			}
			loadedRecordings++;
			statusLoadRecordings();
			if (loadedRecordings >= recordingsToLoad) {
				findDuplicates();
			}
		} else {
			addLoadTask(thread.task);
		}

	}

	private void loadRecordings() {
		loadedRecordings = 0;
		recordingsToLoad = recordings.size();
		statusLoadRecordings();
		for (Recording recording : recordings.values()) {
			addLoadTask(new LoadTask(API_URL + "program.sl?programid=" + recording.programId + "&ajax=true",
					new LoadEvent() {
						public void loadCompleted(LoaderThread thread, boolean success) {
							recordingLoaded(thread, success);
						}
					}));
		}
	}

	private void statusLoadFolders() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (!loggedIn)
					return;
				statusLabel.setText("Ladataan kansioita: " + loadedRecordings + " ladattu");
			}
		});
	}

	private void statusLoadRecordings() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (!loggedIn)
					return;
				try {
					statusLabel.setText("Ladataan ohjelmien tietoja: " + loadedRecordings + "/" + recordingsToLoad
							+ " (" + (100 * loadedRecordings / recordingsToLoad) + "%)");
				} catch (ArithmeticException e) {
					statusLabel.setText("Ladataan ohjelmien tietoja: " + loadedRecordings + " ladattu");
				}
			}
		});
	}

	private void findDuplicates() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				statusLabel.setText("Etsitään duplikaatteja");
			}
		});
		List<Recording> temp = new ArrayList<Recording>(recordings.values());
		while (!temp.isEmpty()) {
			Recording recording = temp.remove(0);
			Duplicate duplicate = new Duplicate();
			recording.duplicate = duplicate;
			duplicate.name = recording.name;
			duplicate.parts.add(recording);
			for (Recording other : new ArrayList<Recording>(temp)) {
				if (recording.duplicates(other)) {
					other.duplicate = duplicate;
					duplicate.parts.add(other);
					temp.remove(other);
				}
			}
			if (duplicate.parts.size() > 1) {
				duplicates.add(duplicate);
			}
		}
		Collections.sort(duplicates, new Comparator<Duplicate>() {
			public int compare(Duplicate o1, Duplicate o2) {
				int partsCompare = Integer.compare(o2.parts.size(), o1.parts.size());
				return (partsCompare != 0) ? partsCompare : o1.name.compareTo(o2.name);
			}
		});
		for (Duplicate duplicate : duplicates) {
			Collections.sort(duplicate.parts, new Comparator<Recording>() {
				public int compare(Recording o1, Recording o2) {
					return o1.date.compareTo(o2.date);
				}
			});
		}
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				refreshDuplicatesList();
			}
		});
	}

	private void refreshDuplicatesList() {
		for (Duplicate duplicate : duplicates) {
			duplicatesModel.addElement(duplicate);
		}
		statusLabel.setText("Valmis");
	}

	private void loadFolders() {
		loadedRecordings = 0;
		recordingsToLoad = 1;
		statusLoadFolders();
		addLoadTask(new LoadTask(API_URL + "ready.sl?folderid=&ajax=true", new LoadEvent() {
			public void loadCompleted(LoaderThread thread, boolean success) {
				folderLoaded(thread, success);
			}
		}));
	}

	private void addLoadTask(LoadTask task) {
		mainLock.lock();
		try {
			tasks.add(task);
			noTasks.signal();
		} finally {
			mainLock.unlock();
		}
	}

	private void interruptThreads() {
		for (int i = 0; i < THREADS; i++) {
			loaderThreads[i].interrupt();
		}
	}

	private void createThreads() {
		for (int i = 0; i < THREADS; i++) {
			loaderThreads[i] = new LoaderThread(i);
			loaderThreads[i].start();
		}
	}

	private URI getUri(String str) {
		try {
			return new URI(str);
		} catch (Exception e) {
			showError(e);
			return null;
		}
	}

	private void showError(Throwable t) {
		final StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		Runnable r = new Runnable() {
			public void run() {
				JOptionPane.showMessageDialog(ElisaViihdeDupeSearch.this, sw.toString(), "Virhe",
						JOptionPane.ERROR_MESSAGE);
			}
		};
		if (SwingUtilities.isEventDispatchThread())
			r.run();
		else
			SwingUtilities.invokeLater(r);
	}

	private class LoaderThread extends Thread {
		volatile String result;
		volatile boolean cancel = false;
		URI uri;
		LoadTask task = null;

		public LoaderThread(int num) {
			super("HTTP-lataajasäie " + num);
		}

		@Override
		public void run() {
			do {
				try {
					if (tasks.isEmpty()) {
						try {
							mainLock.lock();
							noTasks.await();
							mainLock.unlock();
						} catch (InterruptedException e) {
							mainLock.unlock();
							return;
						}
					}
					if (cancel)
						return;
					task = tasks.poll();
					if (task == null)
						continue;
					uri = getUri(task.uri);
					BufferedReader reader = null;
					HttpClient client = HttpClients.createDefault();
					HttpGet get = new HttpGet(uri);
					if (httpContext == null) {
						httpContext = new BasicHttpContext();
						cookieStore = new BasicCookieStore();
						httpContext.setAttribute("http.cookie-store", cookieStore);
					}
					CloseableHttpResponse response = (CloseableHttpResponse) client.execute(get, httpContext);
					InputStream in = response.getEntity().getContent();
					reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
					StringBuffer buffer = new StringBuffer();
					String read = "";
					while (read != null) {
						buffer.append(read);
						read = reader.readLine();
					}
					reader.close();
					result = buffer.toString();
					task.complete.loadCompleted(this, true);
				} catch (Exception e) {
					showError(e);
					task.complete.loadCompleted(this, false);
				}
			} while (!cancel);
		}
	}

	private class LoadTask {
		public String uri;
		public LoadEvent complete;

		public LoadTask(String uri, LoadEvent complete) {
			this.uri = uri;
			this.complete = complete;
		}
	}

	private interface LoadEvent {
		abstract void loadCompleted(LoaderThread thread, boolean success);
	}

	public static final DateFormat LIST_FORMAT = new SimpleDateFormat("d.M.yyyy");
	public static final DateFormat DETAILS_FORMAT = new SimpleDateFormat("d.M.yyyy H:mm:ss");

	public static final DateFormat JSON_FORMAT = new SimpleDateFormat("d.M.yyyy HH:mm:ss");

	private class Recording {
		public String programId, programViewId, name, description, folderId, dateString, channel, folderName;
		public int viewCount;
		public Duration duration;
		public Date date;
		public Duplicate duplicate;

		public void update(Map<String, String> folderNames) {
			try {
				folderName = folderNames.get(folderId);
				if (folderName == null)
					folderName = "(ei tiedossa)";
				date = JSON_FORMAT.parse(dateString);
			} catch (Exception e) {}
		}

		public boolean duplicates(Recording other) {
			if (other == null)
				return false;
			if (!name.equals(other.name))
				return false;
			if (other.description == null || description == null)
				return false;
			Pattern p = Pattern.compile("(\\D|^)(\\d+)(\\D|$)");
			Matcher m1 = p.matcher(description);
			Matcher m2 = p.matcher(other.description);
			boolean found1 = m1.find(), found2 = m2.find();
			if (found1 && found2) {
				do {
					if (!m1.group(2).equals(m2.group(2)))
						return false;
					found1 = m1.find();
					found2 = m2.find();
				} while (found1 && found2);
			}
			if (found1 || found2)
				return false;
			String desc1 = description.toLowerCase(), desc2 = other.description.toLowerCase();
			if (desc1.equals(desc2))
				return true;
			if (desc1.length() < THRESHOLD || desc2.length() < THRESHOLD)
				return false;
			String test1 = desc2.substring(0, Math.min(THRESHOLD, desc2.length()));
			String test2 = desc1.substring(0, Math.min(THRESHOLD, desc1.length()));
			return ((desc2.length() > 0 && desc1.contains(test1)) || (desc1.length() > 0 && desc2.contains(test2)));
		}

		public boolean isBetterThan(Recording other) {
			if (other == null)
				return true;
			if (viewCount > 0 && other.viewCount == 0)
				return true;
			if (date.before(other.date))
				return true;
			return false;
		}

		@Override
		public String toString() {
			return (viewCount == 0 ? "\u25CB " : "\u25CF ") + name + " (" + channel + " "
					+ (date == null ? "" : LIST_FORMAT.format(date) + ", ") + duration + ")";
		}
	}

	private class Duration implements Comparable<Duration> {
		public int minutes;
		public String text;

		public Duration(int min) {
			this.minutes = min;
		}

		@Override
		public int compareTo(Duration o) {
			return Integer.compare(minutes, o.minutes);
		}

		@Override
		public String toString() {
			return text == null ? Integer.toString(minutes) : text;
		}

		@Override
		public boolean equals(Object obj) {
			return obj instanceof Duration && minutes == ((Duration) obj).minutes;
		}

		@Override
		public int hashCode() {
			return this.minutes;
		}
	}

	static class Duplicate {
		String name;
		List<Recording> parts = new ArrayList<Recording>();

		@Override
		public String toString() {
			return name + " (" + parts.size() + " kopiota)";
		}

		@Override
		public boolean equals(Object obj) {
			return super.equals(obj);
		}
	}

	private class RefreshableListModel extends DefaultListModel<Duplicate> {

		private static final long serialVersionUID = -2102308642279631291L;

		public void refresh() {
			for (int i = size() - 1; i >= 0; i--) {
				Duplicate d = get(i);
				if (d.parts.size() <= 1)
					removeElement(d);
			}
			if (!isEmpty())
				fireContentsChanged(this, 0, size() - 1);
		}
	}

}
