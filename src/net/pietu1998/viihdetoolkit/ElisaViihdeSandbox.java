package net.pietu1998.viihdetoolkit;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class ElisaViihdeSandbox extends JFrame implements ViihdeApplication {

	private static final long serialVersionUID = 3301999459067675925L;

	public static void main(String[] args) {
		new ElisaViihdeSandbox().kaynnista();
	}

	public void startApplication() {
		kaynnista();
	}

	JTextField komento;
	JTextArea tulos;
	boolean sisalla = false, poistuKunValmis = false;
	String kansio, tulevaKansio;
	CookieStore yleisKeksiVarasto;
	HttpContext yleisKonteksti;
	Lataa tallennin = null;
	long[] luetut;
	int[] maarat;

	public static final int SPEED_BUFFER = 20;

	public ElisaViihdeSandbox() {
		super();
	}

	private void kaynnista() {
		setTitle("Elisa Viihde Sandbox");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setResizable(false);
		setLocationByPlatform(true);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				int vastaus = JOptionPane.showConfirmDialog(ElisaViihdeSandbox.this,
						"Oletko kirjautunut ulos? Se pitäisi tehdä ennen sulkemista.", "Kirjaudu ulos",
						JOptionPane.YES_NO_OPTION);
				if (vastaus == JOptionPane.YES_OPTION)
					System.exit(0);
			}
		});
		try {
			List<Image> icons = new ArrayList<Image>();
			icons.add(ImageIO.read(getClass().getResourceAsStream("/icon-sandbox.png")));
			icons.add(ImageIO.read(getClass().getResourceAsStream("/icon-sandbox16.png")));
			setIconImages(icons);
		} catch (IOException e) {
			virheIlmoitus(e);
			System.exit(1);
		}
		komento = new JTextField();
		tulos = new JTextArea("");
		tulos.setEditable(false);
		JButton laheta = new JButton("Lähetä");
		laheta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				URI u = uri(komento.getText());
				if (u != null) {
					new Lataa(u, new Valmis() {
						public void valmis(final Lataa l) {
							SwingUtilities.invokeLater(new Runnable() {
								public void run() {
									tulos.setText(l.tulos);
								}
							});
						}
					}).start();
				}
			}
		});
		JPanel ylaosa = new JPanel(new BorderLayout());
		ylaosa.add(komento, BorderLayout.CENTER);
		ylaosa.add(nappiPaneeli(), BorderLayout.PAGE_END);
		ylaosa.add(laheta, BorderLayout.LINE_END);
		JScrollPane skrolli = new JScrollPane(tulos);
		add(skrolli, BorderLayout.CENTER);
		add(ylaosa, BorderLayout.PAGE_START);
		setSize(640, 480);
		setVisible(true);
	}

	private JPanel nappiPaneeli() {
		JPanel paneeli = new JPanel(new GridLayout());
		paneeli.add(new ToimintoNappi("Sisään", new Komento() {
			public String komento() {
				return loginUrl();
			}
		}));
		paneeli.add(new ToimintoNappi("Ulos", new Komento() {
			public String komento() {
				return logoutUrl();
			}
		}));
		paneeli.add(new ToimintoNappi("Listaus", new Komento() {
			public String komento() {
				return haeKansioUrl();
			}
		}));
		paneeli.add(new ToimintoNappi("Ohjelma", new Komento() {
			public String komento() {
				return haeOhjelmaUrl();
			}
		}));
		return paneeli;
	}

	private URI uri(String str) {
		try {
			return new URI(str);
		} catch (Exception e) {
			virheIlmoitus(e);
			return null;
		}
	}

	private void virheIlmoitus(Throwable t) {
		final StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		t.printStackTrace(pw);
		Runnable r = new Runnable() {
			public void run() {
				JOptionPane.showMessageDialog(ElisaViihdeSandbox.this, sw.toString(), "Virhe",
						JOptionPane.ERROR_MESSAGE);
			}
		};
		if (SwingUtilities.isEventDispatchThread())
			r.run();
		else
			SwingUtilities.invokeLater(r);
	}

	private String haeKansioUrl() {
		String kansio = JOptionPane.showInputDialog(this, "Kansio-ID (tyhjä = pääkansio):", "Kansio",
				JOptionPane.QUESTION_MESSAGE);
		if (kansio == null)
			return null;
		return API_URL + "ready.sl?folderid=" + kansio + "&ajax=true";
	}

	private String haeOhjelmaUrl() {
		String ohjelma = JOptionPane.showInputDialog(this, "Ohjelma-ID:", "Ohjelma", JOptionPane.QUESTION_MESSAGE);
		if (ohjelma == null)
			return null;
		return API_URL + "program.sl?programid=" + ohjelma + "&ajax=true";
	}

	private String loginUrl() {
		String kayttaja = JOptionPane.showInputDialog(this, "Käyttäjänimi:", "Käyttäjänimi",
				JOptionPane.QUESTION_MESSAGE);
		if (kayttaja == null)
			return null;
		String salasana = JOptionPane.showInputDialog(this, "Salasana:", "Salasana", JOptionPane.QUESTION_MESSAGE);
		if (salasana == null)
			return null;

		return API_URL + "default.sl?username=" + kayttaja + "&password=" + salasana + "&ajax=true";
	}

	private String logoutUrl() {
		return API_URL + "logout.sl?ajax=true";
	}

	private class Lataa extends Thread {
		URI uri;
		String tulos;
		Valmis valmis;

		public Lataa(URI uri, Valmis valmis) {
			this.uri = uri;
			this.valmis = valmis;
		}

		@Override
		public void run() {
			try {
				HttpClient asiakas = HttpClients.createDefault();
				HttpGet get = new HttpGet(uri);
				if (yleisKonteksti == null) {
					HttpContext konteksti = yleisKonteksti = new BasicHttpContext();
					CookieStore keksiVarasto = yleisKeksiVarasto = new BasicCookieStore();
					konteksti.setAttribute("http.cookie-store", keksiVarasto);
				}
				CloseableHttpResponse vastaus = (CloseableHttpResponse) asiakas.execute(get, yleisKonteksti);
				InputStream in = vastaus.getEntity().getContent();
				StringBuffer puskuri = new StringBuffer();
				byte[] b = new byte[512];
				int i;
				while ((i = in.read(b)) > 0) {
					puskuri.append(new String(b, 0, i, Charset.forName("UTF-8")));
				}
				in.close();
				tulos = puskuri.toString();
				valmis.valmis(this);
			} catch (Exception e) {
				virheIlmoitus(e);
				valmis.valmis(this);
			}
		}
	}

	private interface Valmis {
		abstract void valmis(Lataa l);
	}

	private interface Komento {
		public abstract String komento();
	}

	private class ToimintoNappi extends JButton {

		private static final long serialVersionUID = 3751278999930370356L;

		public ToimintoNappi(String toiminto, final Komento komento) {
			super(toiminto);
			addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String komentoStr = komento.komento();
					if (komentoStr != null)
						ElisaViihdeSandbox.this.komento.setText(komentoStr);
				}
			});
		}

	}
}
